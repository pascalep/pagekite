FROM python:2.7.17-alpine3.11

MAINTAINER Pascal Seiler <pascal@lustere.ch>

WORKDIR /usr/local/bin

ADD https://pagekite.net/pk/ /tmp/pagekite.install

RUN apk --no-cache add bash curl && bash /tmp/pagekite.install && \
    apk del curl bash && rm /tmp/pagekite.install

ENTRYPOINT ["/usr/local/bin/python2"]

CMD ["/usr/local/bin/pagekite.py"]
